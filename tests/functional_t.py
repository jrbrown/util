
import unittest
from typesafe import TypeCheck
from functional import PartiallyApplicable


@TypeCheck(int, float)
@PartiallyApplicable(2)
def sumsquared(x1, x2):
    return (x1 + x2) ** 2


class TestPartialApplication(unittest.TestCase):

    def testNormalApplication(self):
        self.assertAlmostEqual(sumsquared(3,4.2), 51.84)

    def testPartialApplication(self):
        new_func = sumsquared(3)
        self.assertEqual(new_func(4.2), 51.84)


if __name__ == "__main__":
    unittest.main()

