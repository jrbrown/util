
# Todo


## Monad

* Unit tests for monadic functions
* Applicative functions
* Generic types and type vars to parameterise monad types
* More monad instances (e.g. functions)


## Core

* Clean up and type annotate functions


## Typesafe

* Clean and improve type annotations

